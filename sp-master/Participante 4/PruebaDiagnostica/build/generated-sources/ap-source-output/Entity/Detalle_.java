package Entity;

import Entity.Factura;
import Entity.Producto;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2020-01-23T09:13:35")
@StaticMetamodel(Detalle.class)
public class Detalle_ { 

    public static volatile SingularAttribute<Detalle, Factura> id_factura;
    public static volatile SingularAttribute<Detalle, Double> precio;
    public static volatile SingularAttribute<Detalle, Producto> id_producto;
    public static volatile SingularAttribute<Detalle, Integer> cantidad;
    public static volatile SingularAttribute<Detalle, Integer> num_detalle;

}
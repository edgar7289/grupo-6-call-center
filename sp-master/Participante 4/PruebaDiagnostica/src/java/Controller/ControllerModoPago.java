/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import EJB.ModoPagoFacadeLocal;
import Entity.ModoPago;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author christian.ramirezusa
 */
@ManagedBean
@SessionScoped
public class ControllerModoPago implements Serializable{
    
    
    @EJB
    private ModoPagoFacadeLocal modoPagoEJB;
    private List<ModoPago> listaModoPago;
    private ModoPago modoPago;
    
    private String mensaje;

    public List<ModoPago> getListaModoPago() {
        return listaModoPago = modoPagoEJB.findAll();
    }

    public void setListaModoPago(List<ModoPago> listaModoPago) {
        this.listaModoPago = listaModoPago;
    }

    public ModoPago getModoPago() {
        return modoPago;
    }

    public void setModoPago(ModoPago modoPago) {
        this.modoPago = modoPago;
    }
    
    @PostConstruct
    public void init(){
        this.modoPago = new ModoPago();
    }
    
    public void limpiar(){
        this.modoPago = new ModoPago();
    }
    
    public void guardar(){
        try {
            this.modoPagoEJB.create(modoPago);
            this.mensaje = "Modo de pago insertado correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al insertar "+e.getMessage();
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void editar(){
        try {
            this.modoPagoEJB.edit(modoPago);
            this.mensaje = "Modo de pago editado correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al editar "+e.getMessage();
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void eliminar(ModoPago mp){
        try {
            this.modoPagoEJB.remove(mp);
            this.mensaje = "Modo de pago eliminado correctamente";
        } catch (Exception e) {
            this.mensaje = "Error al eliminar "+e.getMessage();
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import EJB.ProductoFacadeLocal;
import Entity.Categoria;
import Entity.Producto;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author christian.ramirezusa
 */
@ManagedBean
@SessionScoped
public class ControllerProductos implements Serializable {

    @EJB
    private ProductoFacadeLocal productoEJB;
    private List<Producto> listaProductos;
    private Producto producto;
    private Categoria categoria;

    private String mensaje;

    public List<Producto> getListaProductos() {
        return listaProductos =  productoEJB.findAll();
    }

    public void setListaProductos(List<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @PostConstruct
    public void init() {
        this.categoria = new Categoria();
        this.producto = new Producto();
        
    }

    public void limpiar() {
        this.categoria = new Categoria();
        this.producto = new Producto();        
    }

    public void cargarId(Producto p){
        this.categoria.setId_categoria(p.getId_categoria().getId_categoria());
        this.producto = p;
    }
    
    public void guardar(){
        try {                        
            this.producto.setId_categoria(categoria);
            this.productoEJB.create(producto);
            this.mensaje = "Producto insertado correctamente";            
        } catch (Exception e) {
            this.mensaje = "Error al insertar un producto " +e.getMessage();
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void editar(){
        try {
            //this.producto.setId_categoria(categoria);
            this.productoEJB.edit(producto);
            this.mensaje = "Producto actualizado correctamente";            
        } catch (Exception e) {
            this.mensaje = "Error al actualizar un producto";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void eliminar(Producto p){
        try {
            //this.producto.setId_categoria(categoria);
            this.productoEJB.remove(p);
            this.mensaje = "Producto eliminado";            
        } catch (Exception e) {
            this.mensaje = "Error al eliminar un producto";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}

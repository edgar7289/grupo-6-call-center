/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entity.Factura;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author christian.ramirezusa
 */
@Stateless
public class FacturaFacade extends AbstractFacade<Factura> implements FacturaFacadeLocal {

    @PersistenceContext(unitName = "PruebaDiagnosticaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FacturaFacade() {
        super(Factura.class);
    }
    
    @Override
    public void call(Factura f){
        //StoredProcedureQuery sp = em.createStoredProcedureQuery("sp_insercion_factura(?,?,?,?)");
        //Query query = em.createNamedStoredProcedureQuery("sp_insercion_factura(?,?,?,?)");
        Query query = em.createStoredProcedureQuery("sp_insercion_factura");
        try {                        
            query.setParameter(1, f.getNum_factura());
            query.setParameter(2, f.getFecha());
            query.setParameter(3, f.getId_cliente().getId_cliente());
            query.setParameter(4, f.getNum_pago().getNum_pago()); 
            query.executeUpdate();
            //System.out.println(query);
            if(query.executeUpdate() == 1){
                System.out.println("Se ejecuto el Procedimiento Almacenado");
            }
        } catch (Exception e) {
            System.out.println(query);
            System.out.println("Error" +e.getMessage());
        }
    }
    
    @Override
    public void call2(Factura f){
        
        StoredProcedureQuery sp = em.createNamedStoredProcedureQuery("insert");
        //Query query = em.createNamedStoredProcedureQuery("sp_insercion_factura(?,?,?,?)");
        
        
        /*
            Procedimiento almacenado no me funciona si la PK es VARCHAR
            agregar un campo mas a la base y hare un ID Factura como PK y VARCHAR como correlativo de la Factura
        */
        try {                        
            sp.setParameter("1", f.getNum_factura());
            sp.setParameter("2", f.getFecha());
            sp.setParameter("3", f.getId_cliente().getId_cliente());
            sp.setParameter("4", f.getNum_pago().getNum_pago());             
            //sp.executeUpdate();
            System.out.println(sp);
            if(sp.executeUpdate() == 1){
                System.out.println(sp);
                System.out.println("Se ejecuto el Procedimiento Almacenado");
            }
        } catch (Exception e) {
            System.out.println(sp);
            System.out.println("Error en Facade: " +e.getMessage());
        }
    }
    
    @Override
    public Factura ultimoRegistro(){
        Factura f = new Factura();
        try {
            Query query = em.createNamedQuery("ultimoRegistro");
            List<Factura> list = query.getResultList();            
            f = list.get(0);            
            return f;
        } catch (Exception e) {
            return f;
        }
    }
    
    
}
